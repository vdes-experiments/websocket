import { ApplicationConfig, LoopbackWsApplication } from './application';
export * from './application';
export declare function main(options?: ApplicationConfig): Promise<LoopbackWsApplication>;

import { Client } from '@loopback/testlab';
import { LoopbackWsApplication } from '../..';
export declare function setupApplication(): Promise<AppWithClient>;
export interface AppWithClient {
    app: LoopbackWsApplication;
    client: Client;
}

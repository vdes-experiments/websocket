"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LoopbackWsApplication = void 0;
const tslib_1 = require("tslib");
const http_server_1 = require("@loopback/http-server");
const rest_1 = require("@loopback/rest");
const rest_explorer_1 = require("@loopback/rest-explorer");
const express_1 = tslib_1.__importDefault(require("express"));
const path_1 = tslib_1.__importDefault(require("path"));
const websocket_controller_1 = require("./controllers/websocket.controller");
const sequence_1 = require("./sequence");
const websocket_server_1 = require("./websocket.server");
class LoopbackWsApplication extends rest_1.RestApplication {
    constructor(options = {}) {
        super(options);
        // Set up the custom sequence
        this.sequence(sequence_1.MySequence);
        // Set up default home page
        this.static('/', path_1.default.join(__dirname, '../public'));
        // Customize @loopback/rest-explorer configuration here
        this.configure(rest_explorer_1.RestExplorerBindings.COMPONENT).to({
            path: '/explorer',
        });
        this.component(rest_explorer_1.RestExplorerComponent);
        const expressApp = express_1.default();
        // Create an http server backed by the Express app
        this.httpServer = new http_server_1.HttpServer(expressApp, { ...options.websocket, port: 3000 });
        // Create ws server from the http server
        const wsServer = new websocket_server_1.WebSocketServer(this.httpServer);
        this.bind('servers.websocket.server1').to(wsServer);
        wsServer.use((socket, next) => {
            console.log('Global middleware - socket:', socket.id);
            next();
        });
        // Add a route
        const ns = wsServer.route(websocket_controller_1.WebSocketController, /^\/chats\/\d+$/);
        ns.use((socket, next) => {
            console.log('Middleware for namespace %s - socket: %s', socket.nsp.name, socket.id);
            next();
        });
        this.wsServer = wsServer;
    }
    start() {
        return this.wsServer.start();
    }
    stop() {
        return this.wsServer.stop();
    }
}
exports.LoopbackWsApplication = LoopbackWsApplication;
//# sourceMappingURL=application.js.map
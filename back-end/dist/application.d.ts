import { ApplicationConfig } from '@loopback/core';
import { HttpServer } from '@loopback/http-server';
import { RestApplication } from '@loopback/rest';
import { WebSocketServer } from './websocket.server';
export { ApplicationConfig };
export declare class LoopbackWsApplication extends RestApplication {
    readonly httpServer: HttpServer;
    readonly wsServer: WebSocketServer;
    constructor(options?: ApplicationConfig);
    start(): Promise<void>;
    stop(): Promise<void>;
}

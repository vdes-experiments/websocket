import React, { useState, useEffect } from 'react'
import './App.css'


const io = require('socket.io-client')
// const messages = []

function App() {
  const [messages, setMessages] = useState([]) // {content, sender}
  const [messageText, setMessageText] = useState('')
  const [id, setID] = useState({})
  const [friends, setFriends] = useState([])
  const [friendText, setFriendText] = useState('')
  const socket = io('ws://192.168.43.74:3000/chats/1')

  useEffect(()=>{
    socket.on('chat_room', (msg) => {
      setMessages([...messages, msg])
      setMessageText('')
    })
  }, [socket])


  return (
    <div className="App">
      <ul id="messages">
        {messages.map(message => (
          <li>{message.sender} : {message.content}</li>
        ))}
      </ul>

      <div>
        <input id="m" onChange={e => setMessageText(e.target.value)} />
        <button onClick={() => {
          socket.send({
            topic: 'chat_room',
            body: { content: messageText, sender: id }
          })
          //setMessages([...messages, { content: messageText, sender: id }])
          //setMessageText('')
        }}>Send</button>
      </div>

      <input id="idSender" onChange={e => setID(e.target.value)} /><button onClick={() => console.log(id)}>Enter your ID</button>
      <input id="idAdd" onChange={e => setFriendText(e.target.value)} /><button onClick={() => {
        setFriends([...friends, friendText])
        setFriendText('')
      }}>Enter your friend's ID to add</button>
      <ul>
        {friends.map(friend => (
          <li key={Math.random}>{friend}</li>
        ))}
      </ul>
    </div>
  )
}

export default App;
